// (function(cordova){
//    var EMS = function() {};
//
//    EMS.prototype.isGPSEnabled = function(success, fail) {
//      return cordova.exec(function(args) {
//          success(args);
//      }, function(args) {
//          fail(args);
//      }, 'EMS', 'isGPSEnabled', []);
//    };
//
//    EMS.prototype.goToGPSSettings = function(success, fail) {
//      return cordova.exec(function(args) {
//          success(args);
//      }, function(args) {
//          fail(args);
//      }, 'EMS', 'goToGPSSettings', []);
//    };
//
//    EMS.prototype.checkGPS = function(success, fail) {
//      return cordova.exec(function(args) {
//          success(args);
//      }, function(args) {
//          fail(args);
//      }, 'EMS', 'checkGPS', []);
//    };
//
//    window.EMS = new EMS();
//
//    // backwards compatibility
//    window.plugins = window.plugins || {};
//    window.plugins.EMS = window.EMS;
//})(window.PhoneGap || window.Cordova || window.cordova);


var exec = require('cordova/exec');

var LOG_TAG = "EMSBASE.js PLUGIN";

var EMS = {
	testVar: 'its test ems var',
	init: function (successCallback) {
        console.log(LOG_TAG + ' call init()');
		exec(function(data){
            console.log(LOG_TAG + ': succ: ' + data)
            if(successCallback !== null || typeof successCallback !== 'undefined'){
                var d = data;
                if(typeof d.data !== 'undefined'){
                    d.data = JSON.parse(d.data);
                }
                successCallback(d);
            }
        }, function(data){
            console.log(LOG_TAG + ': err: ' + data)
            if(successCallback !== null || typeof successCallback !== 'undefined'){
                successCallback(data);
            }
        }, "EMS", "init", []);
	},

	sendPacket: function (eventName, data, successCallback, errorCallback) {
		exec(function(){
		    successCallback || successCallback();
		}, function(){
            errorCallback || errorCallback();
        }, "EMS", "sendPacket", [eventName, data]);
	},

	checkGPS: function () {
		exec(null, null, "EMS", "checkGPS", []);
	}
};
console.log(LOG_TAG + 'initing...');

module.exports = EMS;