package com.etrans.cordova.plugin.emsbase;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

public class PacketManager{

    private static PacketManager instance;
    private static CallbackContext callbackContext;
//    private static ArrayList<Packet> packetList;

    private PacketManager(){}
    public static PacketManager getInstance(){
        if(instance == null){
            instance = new PacketManager();
        }
        return instance;
    }

    public void sendPacket(CallbackContext callbackContext, Packet packet){

    }

    public static void setCallback(CallbackContext callbackContext){
        PacketManager.callbackContext = callbackContext;
        System.out.println("PacketManager setCallback");
    }

    public static void receiveAnsver(String eventName){
        System.out.println("PacketManager receive: " + eventName);
        if(callbackContext != null) {
            PluginResult result = new PluginResult(PluginResult.Status.OK, eventName);
            result.setKeepCallback(true);
            PacketManager.callbackContext.sendPluginResult(result);
        }
    }

}