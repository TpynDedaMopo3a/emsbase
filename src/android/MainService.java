package com.etrans.cordova.plugin.emsbase;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class MainService extends Service {

    final static String LOG_TAG = "EmsBaseService";
    private final String serverAddress = "http://kolizej.no-ip.org:12345/";
    private static Socket socket;
    private String MY_SEND_PACKET_ACTION = "SENDPACKETACTION";
    private static String MY_RECEIVE_PACKET_ACTION = "RECEIVEPACKETACTION";
    private MySendPacketReceiver mySendPacketReceiver;
    private static MainService instance;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");

        instance = this;

        try {
            IO.Options options = new IO.Options();
            options.forceNew = true;
            socket = IO.socket(serverAddress, options);

            socket.on("disconnect", new SocketDisconnectAction());

            socket.on("login:failed", new SocketLoginFailedAction());
            socket.on("login:success", new SocketLoginSuccessAction());

            socket.on("get jobs:success", new SocketGetJobsSuccessAction());

            mySendPacketReceiver = new MySendPacketReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(MY_SEND_PACKET_ACTION);
            registerReceiver(mySendPacketReceiver, intentFilter);

            socket.connect();
        } catch (URISyntaxException e) {
            Log.d(LOG_TAG, "error create connection: " + e.toString());
        }
//        java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
//        java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
//        Configs.setContext(this.getBaseContex());
//        if (remoteController == null) {
//            remoteController = RemoteController.getInstance();
//        }
//        remoteController.start();
        return START_STICKY;
    }

    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
        unregisterReceiver(mySendPacketReceiver);
        Log.d(LOG_TAG, "onDestroy");
//        if (remoteController != null) {
//            remoteController.stop();
//        }
    }

    public static boolean sendPacket(String eventName) {
        if (socket != null && socket.connected()) {
            socket.emit(eventName);
            return true;
        } else {
            return false;
        }
    }

    public static boolean sendPacket(String eventName, String data) {
        if (socket != null && socket.connected()) {
            if (data == null) {
                socket.emit(eventName);
            } else {
                try {
                    socket.emit(eventName, new JSONObject(data));
                }
                catch (JSONException exception){}
            }
            return true;
        } else {
            return false;
        }
    }

    public static void sendCallback(String eventName) {
        Log.d(LOG_TAG, "sendCallback: " + eventName);
        Intent intent = new Intent();
        intent.setAction(MY_RECEIVE_PACKET_ACTION);
        intent.putExtra("EVENT_NAME", eventName);
        if (instance != null) {
            Log.d(LOG_TAG, "instance is exists sending");
            instance.sendBroadcast(intent);
        }
    }

    public static void sendCallback(String eventName, String data) {
        Log.d(LOG_TAG, "sendCallback: " + eventName);
        Intent intent = new Intent();
        intent.setAction(MY_RECEIVE_PACKET_ACTION);
        intent.putExtra("EVENT_NAME", eventName);
        intent.putExtra("EVENT_DATA", data);
        if (instance != null) {
            Log.d(LOG_TAG, "instance is exists sending");
            instance.sendBroadcast(intent);
        }
    }

    private class MySendPacketReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            String eventName = arg1.getStringExtra("EVENT_NAME");
            String eventData = arg1.getStringExtra("EVENT_DATA");

            MainService.sendPacket(eventName, eventData);

            Toast.makeText(MainService.this,
                    "Triggered by Service!\n"
                            + "Data passed: " + String.valueOf(eventName),
                    Toast.LENGTH_LONG).show();

        }

    }

}
