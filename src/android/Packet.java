package com.etrans.cordova.plugin.emsbase;

import org.json.JSONException;
import org.json.JSONObject;

public class Packet {

    private String eventName;
    private JSONObject data;

    public Packet(String eventName){
        this.eventName = eventName;
    }

    public Packet(String eventName, JSONObject data) {
        this.eventName = eventName;
        this.data = data;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
